package junia.lab08.web.controller;

import junia.lab08.web.dto.Slide;
import junia.lab08.web.transformation.SlideDecoder;
import junia.lab08.web.transformation.SlideEncoder;
import junia.lab08.web.utils.CurrentSlideHolder;
import junia.lab08.web.utils.SessionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.websocket.*;
import jakarta.websocket.server.ServerEndpoint;
import java.io.IOException;

@ServerEndpoint(
        value = "/control",
        decoders = { SlideDecoder.class },
        encoders = { SlideEncoder.class })
public class RevealController {

    private static final Logger LOGGER = LoggerFactory.getLogger(RevealController.class);


    @OnOpen
    public void open(Session s) throws IOException, EncodeException {
        SessionManager.getInstance().addSession(s);
        SessionManager.getInstance().sendToSession(s, CurrentSlideHolder.getInstance().getCurrentSlide());
    }

    @OnClose
    public void close(Session s){
        SessionManager.getInstance().removeSession(s);
    }


    @OnMessage
    public void onSlide(Slide slide, Session session) {
        //TODO implement
    }



}
